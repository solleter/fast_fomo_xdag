import stringdist
import random

address_list = [
    "YLjTuf08zCCztdbu8yQ10csU0Sk3v4EH",
    "aGmNA57IiBtKMY132Mt3730W6KKkOhIn",
    "43m7TpEkcLKXjuh13J7nn/5tOFBioW4D",
    "K3gwPfCjaklMCg7Tl1g3+B0dGIu54AtC",
    "jHwCRVkRKpTykg0pzL334H9dKnm2AHp7",
    "QJw9y/7zCpiPI2OklQ56q8fCw6P5vBNS",
    "5K7zvVBycAEK355nM4Df46wXEZ0exhgl",
    "sz9sU9MRV108dKj7pIrCZZ2V8qif2FSc",
    "5Pt+4eHyVWVIf5kPSrMr4Q/JYRbr/hcB",
    "3wwjcQD3eDj00dFat8GTl7FtOrjsCrHo",
]

jackpot_address = "YLjTuf08zCCztdbu8yQ10csU0Sk3v49-"

obj_list = []


class DistanceObj(object):
    def __init__(self, _address, _distance):
        self.address = _address
        self.distance = _distance
        self.rand = random.randint(100, 500)


for address in address_list:
    distance = stringdist.levenshtein(jackpot_address, address)
    print(address, distance)
    data = {"address": address, "distance": distance, "rand": random.randint(100,500)}
    obj_list.append(data)


def debug_list(_list):
    for item in _list:
        print(item["address"], item["distance"], item["rand"])


debug_list(obj_list)
obj_list.sort(key=lambda x: x["rand"])
obj_list.sort(key=lambda x: x["distance"])
print("====================")
debug_list(obj_list)






