import json
import subprocess


def do_command(_method, _param_list=None):
    command = ["bash", "{}.sh".format(_method)]

    try:
        if _param_list is not None:
            for param in _param_list:
                command.append(str(param))

        result = subprocess.check_output(command).decode("utf-8")
        result = result.replace("\n","")
        result = result.replace("\t","")
        result = "{" + result

        json_obj = json.loads(result)
        return json_obj
    except:
        return None


result = do_command("xdag_version", [])
