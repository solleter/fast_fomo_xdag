from peewee import *
import util

db_root = "./db/"
db_play_path = db_root + "play.db"
db_round_path = db_root + "round.db"
db_allocation_path = db_root + "allocation.db"
db_bonus = db_root + "bonus.db"
db_payment = db_root + "payment.db"

db_play = SqliteDatabase(db_play_path)
db_round = SqliteDatabase(db_round_path)
db_allocation = SqliteDatabase(db_allocation_path)
db_bonus = SqliteDatabase(db_bonus)
db_payment = SqliteDatabase(db_payment)

curr_round_surplus_time = -1        # >= 0 normal, -1 end, -2 data un complete


class PlayAllocation(object):
    def __init__(self, _p_bonus, _p_prev_player, _p_dev):
        self.bonus_percentage = _p_bonus
        self.prev_player_percentage = _p_prev_player
        self.dev_percentage = _p_dev

        # runtime variables
        self.curr_play_amount = 0.0
        self.to_bonus_amount = 0.0
        self.to_prev_player_amount = 0.0
        self.to_dev_amount = 0.0

    def set_runtime_data(self, _play_amount):
        self.curr_play_amount = _play_amount
        self.to_bonus_amount = util.get_precision(self.curr_play_amount * self.bonus_percentage, 6)
        self.to_prev_player_amount = util.get_precision(self.curr_play_amount * self.prev_player_percentage, 6)
        self.to_dev_amount = util.get_precision(self.curr_play_amount * self.dev_percentage, 6)

    def calculate_prev_player_reward(self, _total_prev_amount, _prev_player_amount):
        percentage = util.get_precision(_prev_player_amount / _total_prev_amount, 3)
        reward_amount = self.to_prev_player_amount * percentage
        reward_amount = util.get_precision(reward_amount, 6)
        return reward_amount, percentage


class BonusAllocation(object):
    def __init__(self, _p_jackpot, _lucky_players, _p_dev, _p_next_round):
        self.jackpot = _p_jackpot
        self.lucky_players = _lucky_players
        self.dev = _p_dev
        self.next_round = _p_next_round

        self.to_jackpot = 0.0
        self.to_lucky_players = 0.0
        self.to_dev = 0.0
        self.to_next_round = 0.0

    def set_runtime_data(self, _total_bonus):
        _total_bonus = util.get_precision(_total_bonus, 6)
        self.to_jackpot = util.get_precision(_total_bonus * self.jackpot, 6)
        self.to_lucky_players = util.get_precision(_total_bonus * self.lucky_players, 6)
        self.to_dev = util.get_precision(_total_bonus * self.dev, 6)
        self.to_next_round = util.get_precision(_total_bonus * self.next_round, 6)


play_allocation = None
bonus_allocation = None


class PaymentObj(Model):
    payment_id = CharField()
    to_address = CharField()
    amount = FloatField()
    payment_type = IntegerField()       # 0 dividend, 1 dev, 2 bonus-jackpot, 3 bonus-lucky player, 4 bonus-dev
    transaction_id = CharField(default="")
    round = IntegerField()

    class Meta:
        database = db_payment


class PlayObj(Model):
    play_id = CharField(unique=True)
    join_str_time = CharField()         # for view.
    join_timestamp = IntegerField()     # for check is the game need end? compare with utc timestamp of now.
    join_longtime = IntegerField()      # for compare the play order.
    player_address = CharField()
    amount = FloatField()
    amount_to_bonus = FloatField(default=0)
    amount_to_prev_player = FloatField(default=0)
    amount_to_dev = FloatField(default=0)
    dividend_allocated = BooleanField()
    total_dividend_reward = FloatField(default=0)
    play_end = BooleanField(default=False)
    round = IntegerField(default=-1)

    class Meta:
        database = db_play


# class DividendAllocationObj(Model):
#     from_play_id = CharField()
#     to_play_id = CharField(default="")
#     to_address = CharField(default="")
#     total_prev_play = IntegerField(default=0)
#     total_prev_amount = FloatField(default=0.0)
#     curr_play_amount = FloatField(default=0)
#     curr_play_percentage = FloatField(default=0)
#     payment_amount = FloatField()
#     payment_id = CharField(default="")
#     allocation_type = IntegerField()        # 0 prev player, 1 pool, 2 dev,
#     update_at = DateTimeField(default=util.get_now_utc_time())
#
#     class Meta:
#         database = db_allocation


# class BonusAllocationObj(Model):
#     allocation_id = CharField()  # for play, jackpot.play_id + play.play_id, for dev, jackpot.play_id + dev address
#     to_play_id = CharField(default="")
#     to_address = CharField()
#     play_amount = FloatField()
#     dividend_reward = FloatField()
#     payment_amount = FloatField()
#     payment_id = CharField(default="")
#     bonus_type = IntegerField()             # 0 jackpot, 1 lucky player, 2 dev
#     round = IntegerField()
#
#     class Meta:
#         database = db_bonus


class GameRoundObj(Model):
    round = IntegerField()
    start_str_time = CharField()
    start_timestamp = IntegerField()
    start_longtime = IntegerField()
    total_play = IntegerField()
    total_join_amount = FloatField()
    dividend_to_prev_player_amount = FloatField()
    dividend_to_dev_amount = FloatField()
    dividend_to_bonus = FloatField()
    bonus_from_prev_round = FloatField()
    total_bonus_amount = FloatField()
    bonus_to_jackpot_amount = FloatField()
    bonus_to_lucky_player_amount = FloatField()
    bonus_to_dev_amount = FloatField()
    bonus_to_next_round_amount = FloatField()
    jackpot_play_id = CharField()
    jackpot_player_address = CharField()
    jackpot_play_join_amount = FloatField()
    end_str_time = CharField()
    end_timestamp = IntegerField()

    class Meta:
        database = db_round


def calculate_dividend(_play_amount):
    play_allocation.set_runtime_data(_play_amount)
    to_bonus_amount = play_allocation.to_bonus_amount
    to_prev_player_amount = play_allocation.to_prev_player_amount
    to_dev_amount = play_allocation.to_dev_amount
    return to_bonus_amount, to_prev_player_amount, to_dev_amount


def update_surplus_time(_surplus_time):
    global curr_round_surplus_time
    curr_round_surplus_time = _surplus_time


def init_model():
    global play_allocation, bonus_allocation
    play_allocation = PlayAllocation(0.55, 0.4, 0.05)
    bonus_allocation = BonusAllocation(0.65, 0.2, 0.05, 0.1)
