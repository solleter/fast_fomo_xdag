from model import PlayObj, GameRoundObj, PaymentObj
from peewee import *
import os
import model
import log

__play_dict = dict()
__play_list = []
__dividend_allocation_list = []
__bonus_allocation_list = []
__round_list = []
curr_playing_round = -1
prev_game_round = None


def has_play(_play_id):
    global __play_list
    if _play_id in __play_dict:
        return True

    try:
        obj = PlayObj.get(PlayObj.play_id == _play_id)
    except DoesNotExist:
        obj = None

    if obj is None:
        return False
    return True


def save_play(_play_obj):
    global __play_list
    if _play_obj not in __play_list:
        __play_list.append(_play_obj)

    if _play_obj.play_id not in __play_dict:
        __play_dict[_play_obj.play_id] = _play_obj

    _play_obj.save()


def save_play_list(_play_obj_list):
    global __play_list
    if _play_obj_list is None or len(_play_obj_list) == 0:
        return

    for item in _play_obj_list:
        if item not in __play_list:
            __play_list.append(item)
        if item.play_id not in __play_dict:
            __play_dict[item.play_id] = item
    __play_list.sort(key=lambda x: x.join_timestamp)

    with model.db_play.atomic():
        for obj in _play_obj_list:
            obj.save()


def get_play_list_by_round(_game_round, force_from_database=False):
    selected_list = []
    if not force_from_database:
        for item in __play_list:
            if item.round == _game_round:
                selected_list.append(item)
    else:
        result = PlayObj.select().where(PlayObj.round == _game_round)
        for item in result:
            selected_list.append(item)
    return selected_list


def get_un_end_play(force_from_database=False):
    """
    Get All un end play
    :return: [PlayObj]
    """
    global __play_list
    selected_list = []
    if not force_from_database:
        for item in __play_list:
            if item.play_end == 0:
                selected_list.append(item)
    else:
        result = PlayObj.select().where(PlayObj.play_end == 0)
        for item in result:
            selected_list.append(item)
    selected_list.sort(key=lambda x: x.join_timestamp)
    return selected_list


def get_un_end_play_before_timestamp(_timestamp):
    global __play_list
    selected_list = []
    for item in __play_list:
        if item.play_end == 0 and item.join_timestamp < _timestamp:
            selected_list.append(item)
    return selected_list

    # result = PlayObj.select().where(
    #    (PlayObj.play_end == 0) & (PlayObj.join_timestamp < _timestamp))
    # return result


def get_un_allocate_play_list():
    global __play_list
    selected_list = []
    for item in __play_list:
        if item.play_end == 0 and item.dividend_allocated == 0:
            selected_list.append(item)
    return selected_list

    # result = PlayObj.select().where((PlayObj.play_end == 0) & (PlayObj.dividend_allocated == 0))
    # return result


def get_un_allocated_play_list_before_timestamp(_timestamp):
    global __play_list
    selected_list = []
    print("Count: ", len(__play_list))
    for item in __play_list:
        if item.play_end == 0 and item.join_timestamp < _timestamp and item.dividend_allocated == 0:
            selected_list.append(item)
    return selected_list

    # result = PlayObj.select().where((PlayObj.play_end == 0) & (PlayObj.join_timestamp < _timestamp))
    # return result


def save_allocation(_allocation_obj):
    global __dividend_allocation_list
    if _allocation_obj is None:
        return

    if _allocation_obj not in __dividend_allocation_list:
        __dividend_allocation_list.append(_allocation_obj)

    _allocation_obj.save()


def save_allocation_list(_allocation_obj_list):
    if _allocation_obj_list is None or len(_allocation_obj_list) == 0:
        return
    global __dividend_allocation_list
    for item in _allocation_obj_list:
        if item not in __dividend_allocation_list:
            __dividend_allocation_list.append(item)

    with model.db_allocation.atomic():
        for obj in _allocation_obj_list:
            obj.save()


def save_bonus_allocation(_bonus_allocation_obj):
    if _bonus_allocation_obj is None:
        return
    global __bonus_allocation_list
    if _bonus_allocation_obj not in __bonus_allocation_list:
        __bonus_allocation_list.append(_bonus_allocation_obj)

    _bonus_allocation_obj.save()


def save_bonus_allocation_list(_bonus_allocation_list):
    if _bonus_allocation_list is None or len(_bonus_allocation_list) == 0:
        return
    global __bonus_allocation_list
    for item in _bonus_allocation_list:
        if item not in __bonus_allocation_list:
            __bonus_allocation_list.append(item)

    with model.db_bonus.atomic():
        for obj in _bonus_allocation_list:
            obj.save()


def get_play_count():

    count = 0
    for item in __play_list:
        if item.play_end == 0:
            count += 1
    return count

    # result = PlayObj.select(fn.Count(PlayObj.play_id)).where(
    #    PlayObj.play_end == 0).scalar()
    # return result


def get_max_amount_play_list():
    global __play_list
    __play_list.sort(key=lambda x: x.amount, reverse=True)
    if len(__play_list) == 0:
        return []

    max_amount = __play_list[0].amount
    selected_list = []
    for item in __play_list:
        if item.amount >= max_amount:
            selected_list.append(item)
        else:
            break
    return selected_list


def get_un_payment_allocation_list():
    global __dividend_allocation_list
    selected_list = []
    for item in __dividend_allocation_list:
        if item.payment_id == "":
            selected_list.append(item)
    return selected_list

    # result = DividendAllocationObj.select().where(DividendAllocationObj.payment_id == "")
    # return result


def get_un_payment_bonus_allocation_list():
    global __bonus_allocation_list
    selected_list = []
    for item in __bonus_allocation_list:
        if item.payment_id == "":
            selected_list.append(item)
    return selected_list

    # result = BonusAllocationObj.select().where(BonusAllocationObj.payment_id == "")
    # return result


def save_payment_list(_payment_list):
    if _payment_list is None or len(_payment_list) == 0:
        return
    with model.db_payment.atomic():
        for payment in _payment_list:
            payment.save()


def get_un_payment_list():
    result = PaymentObj.select().where(PaymentObj.transaction_id == "").limit(100)
    return result


def is_all_round_payment_down(_round):
    result = PaymentObj.select().where((PaymentObj.round == _round) & (PaymentObj.transaction_id == ""))
    if len(result) > 0:
        return False
    return True


def get_payment_list_by_round(_round):
    result = PaymentObj.select().where(PaymentObj.round == _round).order_by(PaymentObj.amount.desc())
    return result


def save_game_round(_game_round):
    global curr_playing_round, prev_game_round
    curr_playing_round += 1
    prev_game_round = _game_round
    _game_round.save()


def get_game_round(_game_round):
    result = GameRoundObj.select().where(GameRoundObj.round == _game_round)
    if len(result) > 0:
        return result[0]
    return None


def get_game_round_id_list():
    result = GameRoundObj.select()
    round_list = []
    for item in result:
        round_list.append(item.round)
    return round_list


def get_bonus_from_prev_round():
    """
    Get "The bonus for next round" from prev game round
    :return:
    """
    if prev_game_round is not None:
        return prev_game_round.bonus_to_next_round_amount
    else:
        return 0


def get_curr_round_start_timestamp():
    if len(__play_list) == 0:
        return -1
    for item in __play_list:
        if item.play_end == 0:
            return item.join_timestamp
    return -1


def __load_curr_round():
    global curr_playing_round, prev_game_round
    result = GameRoundObj.select().order_by(GameRoundObj.round.desc())
    if len(result) == 0:
        curr_playing_round = 1
    else:
        curr_playing_round = result[0].round + 1
        prev_game_round = result[0]


def __load_play_list():
    global __play_list, __play_dict
    __play_list = []
    result = PlayObj.select()
    for item in result:
        __play_list.append(item)
        __play_dict[item.play_id] = item


def __load_round_list():
    global __round_list
    __round_list = []
    result = GameRoundObj.select()
    for item in result:
        __round_list.append(item)


def init_database():
    if not os.path.exists(model.db_root):
        os.mkdir(model.db_root)

    model.db_play.connect()
    model.db_play.create_tables([PlayObj])

    model.db_round.connect()
    model.db_round.create_tables([GameRoundObj])

    model.db_payment.connect()
    model.db_payment.create_tables([PaymentObj])

    __load_curr_round()
    __load_play_list()
    __load_round_list()


def release_database():
    model.db_play.close()
    model.db_round.close()
    model.db_payment.close()
    log.info("Database closed!")

