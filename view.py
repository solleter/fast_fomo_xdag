import db
import model
import json
import os
import log

# json_root = "./json/"
# json_root = "D:/nginx-1.15.4/html/json/"
json_root = "/var/www/html/json/"


class PlayView(object):
    def __init__(self, _play_obj):
        self.play_id = _play_obj.play_id
        self.player_address = _play_obj.player_address
        self.join_timestamp = _play_obj.join_timestamp
        self.join_str_time = _play_obj.join_str_time
        self.amount = _play_obj.amount
        self.dividend_reward = _play_obj.total_dividend_reward

    def to_json_dict(self):
        return {
            "play_id": self.play_id,
            "player_address": self.player_address,
            "join_timestamp": self.join_timestamp,
            "join_str_time": self.join_str_time,
            "amount": self.amount,
            "dividend_reward": self.dividend_reward
        }


class RoundView(object):
    def __init__(self, _round_obj):
        self.round = -1
        self.start_str_time = "-"
        self.total_play = 0
        self.total_join_amount = 0
        self.dividend_to_prev_player_amount = 0
        self.dividend_to_bonus_amount = 0
        self.dividend_to_dev_amount = 0
        self.bonus_from_prev_round_amount = 0
        self.bonus_to_jackpot_amount = 0
        self.bonus_to_lucky_player_amount = 0
        self.bonus_to_dev_amount = 0
        self.bonus_to_next_round_amount = 0
        self.jackpot_play_id = "-"
        self.jackpot_player_address = "-"
        self.jackpot_play_amount = 0
        self.end_str_time = "-"
        self.__init_data(_round_obj)

    def __init_data(self, _round_obj):
        if _round_obj is not None:
            self.round = _round_obj.round
            self.start_str_time = _round_obj.start_str_time
            self.total_play = _round_obj.total_play
            self.total_join_amount = _round_obj.total_join_amount
            self.dividend_to_prev_player_amount = _round_obj.dividend_to_prev_player_amount
            self.dividend_to_bonus_amount = _round_obj.dividend_to_bonus
            self.dividend_to_dev_amount = _round_obj.dividend_to_dev_amount
            self.bonus_from_prev_round_amount = _round_obj.bonus_from_prev_round
            self.bonus_to_jackpot_amount = _round_obj.bonus_to_jackpot_amount
            self.bonus_to_lucky_player_amount = _round_obj.bonus_to_lucky_player_amount
            self.bonus_to_dev_amount = _round_obj.bonus_to_dev_amount
            self.bonus_to_next_round_amount = _round_obj.bonus_to_next_round_amount
            self.jackpot_play_id = _round_obj.jackpot_play_id
            self.jackpot_player_address = _round_obj.jackpot_player_address
            self.jackpot_play_amount = _round_obj.jackpot_play_join_amount
            self.end_str_time = _round_obj.end_str_time

    def to_json_dict(self):
        return {
            "round": self.round,
            "start_str_time": self.start_str_time,
            "total_play": self.total_play,
            "total_join_amount": self.total_join_amount,
            "dividend_to_prev_player_amount": self.dividend_to_prev_player_amount,
            "dividend_to_dev_amount": self.dividend_to_dev_amount,
            "dividend_to_bonus_amount": self.dividend_to_bonus_amount,
            "bonus_from_prev_round_amount": self.bonus_from_prev_round_amount,
            "bonus_to_jackpot_amount": self.bonus_to_jackpot_amount,
            "bonus_to_lucky_player_amount": self.bonus_to_lucky_player_amount,
            "bonus_to_dev_amount": self.bonus_to_dev_amount,
            "bonus_to_next_round_amount": self.bonus_to_next_round_amount,
            "jackpot_play_id": self.jackpot_play_id,
            "jackpot_player_address": self.jackpot_player_address,
            "jackpot_play_amount": self.jackpot_play_amount,
            "end_str_time": self.end_str_time
        }


class PaymentView(object):
    def __init__(self, _payment_obj):
        self.play_id = _payment_obj.payment_id
        self.to_address = _payment_obj.to_address
        self.amount = _payment_obj.amount
        self.transaction_id = _payment_obj.transaction_id
        self.round = _payment_obj.round
        self.payment_type = _payment_obj.payment_type

    def to_json_dict(self):
        return {
            "play_id": self.play_id,
            "to_address": self.to_address,
            "amount": self.amount,
            "transaction_id": self.transaction_id,
            "round": self.round,
            "payment_type": self.payment_type
        }


# def RoundPaymentView(object):
#
#
#     payment_list = db.get_payment_list_by_round(_game_round)
#     for payment in payment_list:
#         self.bonus_payment_list.append(PaymentView(payment).to_json_dict())


class IndexView(object):
    def __init__(self):
        self.round = -1
        self.surplus_time = -1
        self.start_timestamp = -1
        self.total_play = -1
        self.total_join_amount = 0
        self.amount_to_prev_player = 0
        self.amount_to_bonus = 0
        self.amount_from_prev_round = 0
        self.curr_max_amount_play_id = "-"
        self.curr_max_amount_player_address = "-"
        self.curr_max_play_amount = 0
        self.curr_play_list = []
        self.prev_round = ""
        self.__init_data()

    def __init_data(self):
        un_end_play_list = db.get_un_end_play()

        for play in un_end_play_list:
            if play.amount > self.curr_max_play_amount:
                self.curr_max_play_amount = play.amount
                self.curr_max_amount_play_id = play.play_id
                self.curr_max_amount_player_address = play.player_address

        un_end_play_list.reverse()
        self.round = db.curr_playing_round
        self.surplus_time = model.curr_round_surplus_time
        self.start_timestamp = db.get_curr_round_start_timestamp()
        self.total_play = len(un_end_play_list)

        self.amount_from_prev_round = db.get_bonus_from_prev_round()

        for play in un_end_play_list:
            self.total_join_amount += play.amount
            self.amount_to_prev_player += play.amount_to_prev_player
            self.amount_to_bonus += play.amount_to_bonus
            # if play.amount > self.curr_max_play_amount:
            #     self.curr_max_play_amount = play.amount
            #     self.curr_max_amount_play_id = play.play_id
            #     self.curr_max_amount_player_address = play.player_address
            self.curr_play_list.append(PlayView(play).to_json_dict())
        self.prev_round = RoundView(db.prev_game_round).to_json_dict()

    def to_json_dict(self):
        return {
            "round": self.round,
            "surplus_time": self.surplus_time,
            "start_timestamp": self.start_timestamp,
            "total_play": self.total_play,
            "total_join_amount": self.total_join_amount,
            "amount_to_prev_player": self.amount_to_prev_player,
            "amount_to_bonus": self.amount_to_bonus,
            "amount_from_prev_round": self.amount_from_prev_round,
            "curr_max_amount_play_id": self.curr_max_amount_play_id,
            "curr_max_amount_player_address": self.curr_max_amount_player_address,
            "curr_max_play_amount": self.curr_max_play_amount,
            "curr_play_list": self.curr_play_list,
            "prev_round": self.prev_round
        }


class HistoryView(object):
    def __init__(self, _game_round):
        self.round_info = RoundView(db.get_game_round(_game_round)).to_json_dict()
        self.play_list = []
        self.__init_data(_game_round)

    def __init_data(self, _game_round):
        play_list = db.get_play_list_by_round(_game_round)
        for play in play_list:
            self.play_list.append(PlayView(play).to_json_dict())

    def to_json_dict(self):
        return {
            "round_info": self.round_info,
            "play_list": self.play_list
        }


class HistoryArchiveView(object):
    def __init__(self, _round_id_list):
        self.round_list = []
        self.__init_data(_round_id_list)

    def __init_data(self, _round_id_list):
        for index in _round_id_list:
            round_obj = db.get_game_round(index)
            data = {
                "round": index,
                "url": "round?index={}".format(index),
                "data": RoundView(round_obj).to_json_dict()
            }
            self.round_list.append(data)

    def to_json_dict(self):
        return {
            "round_list": self.round_list
        }


def generate_index_data():
    index_obj = IndexView()
    json_data = index_obj.to_json_dict()
    # print(json_data)
    json_str = json.dumps(json_data)
    path = json_root + "index.json"
    with open(path, "w", encoding="utf-8") as f:
        f.write(json_str)


def generate_history_data():
    curr_round = db.curr_playing_round
    new_round_generated = False
    for round in range(1, curr_round):
        path = json_root + "round/round_{}.json".format(round)
        if not os.path.exists(path):
            new_round_generated = True
            json_data = HistoryView(round).to_json_dict()
            json_str = json.dumps(json_data)
            with open(path, "w", encoding="utf-8") as f:
                f.write(json_str)

    if new_round_generated:
        log.info("There is new round generated, so , to regenerate archive json data!")
        generate_history_archive_data()


def generate_history_archive_data():
    curr_round = db.curr_playing_round
    round_id_list = []
    for round in range(1, curr_round):
        round_id_list.append(round)
    round_id_list.reverse()
    json_data = HistoryArchiveView(round_id_list).to_json_dict()
    json_str = json.dumps(json_data)
    path = json_root + "archive.json"
    with open(path, "w", encoding="utf-8") as f:
        f.write(json_str)


def generate_payment_data():
    curr_round = db.curr_playing_round
    for round in range(1, curr_round):
        path = json_root + "payment/payment_round_{}.json".format(round)
        if not os.path.exists(path):
            if db.is_all_round_payment_down(round):
                payment_list = db.get_payment_list_by_round(round)

                bonus_list = []
                dividend_list = []
                if payment_list is not None and len(payment_list) > 0:
                    for payment in payment_list:
                        if payment.payment_type > 1:
                            bonus_list.append(payment)
                        else:
                            dividend_list.append(payment)

                payment_list = bonus_list + dividend_list

                if payment_list is not None and len(payment_list) > 0:
                    json_data = []
                    for payment in payment_list:
                        if payment.amount > 0:
                            json_data.append(PaymentView(payment).to_json_dict())
                    json_str = json.dumps(json_data)
                    with open(path, "w", encoding="utf-8") as f:
                        f.write(json_str)
