import requests
import json
import time
from decimal import Decimal
import datetime
import log
import urllib
import re
import random

requests.packages.urllib3.disable_warnings()


def get_current_strtime():
    return time.strftime('%Y-%m-%d %H:%M:%S')


def log(log_str):
    sys_time = get_current_strtime()
    slog = '[{0}] {1}'.format(sys_time, log_str)
    log.Info(slog)


def get_format_json(data):
    try:
        json_str = json.dumps(data, indent=4, separators=(',', ':'))
        return json_str
    except:
        log('get fromat json error: {}'.format(data))
        return ''


def utc_str_time_to_utc_13timestamp(_utc_str_time):
    t = time.strptime(_utc_str_time.split(".")[0], "%Y-%m-%d %H:%M:%S")
    year = t.tm_year
    month = t.tm_mon
    day = t.tm_mday
    hour = t.tm_hour
    min = t.tm_min
    sec = t.tm_sec
    dt = datetime.datetime(year, month, day, hour, min, sec)
    timestamp = int(dt.timestamp()) * 1000 + int(_utc_str_time.split(".")[1])
    return timestamp


def utc_13timestamp_to_utc_str_time(_utc_timestamp):
    normal_timestamp = int(_utc_timestamp / 1000)
    dt = datetime.datetime.fromtimestamp(normal_timestamp)
    str_time = dt.strftime("%Y-%m-%d %H:%M:%S") + "." + str(_utc_timestamp % 1000)
    return str_time


def get_now_utc_13timestamp():
    now = datetime.datetime.now()
    utc_now = str(datetime.datetime.utcnow()).split(".")[0] + ".000"
    return utc_str_time_to_utc_13timestamp(utc_now)


def get_precision(value, precision):
    if value < 0:
        sign = -1
    else:
        sign = 1

    value = abs(float(value))

    if value == 0.0:
        return value

    #if precision <= 0:
     #   return int(float(value))

    if precision > 20:
        tmpPrecision = precision + 10
    else:
        tmpPrecision = 20

    decimalStr = '0.' + '0' * tmpPrecision
    valueStr = str(Decimal(value).quantize(Decimal(decimalStr)))

    valueArray = valueStr.split('.')
    integerPart = valueArray[0]
    decimalPart = valueArray[1][0:precision]
    resultValueStr = integerPart + "." + decimalPart
    resultValue = float(resultValueStr)

    return resultValue * sign


def http_post_request(url, params, add_to_headers=None):
    headers = {
        "Accept": "application/json",
        'Content-Type': 'application/json;charset=UTF-8',
        "origin": "https://explorer.geekcash.org",
        "referer": "https://explorer.geekcash.org/address/GaZVcUBuuMPbdqhf6uoR85vkVXYPfcpiLK",
        'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36',
    }
    if add_to_headers:
        headers.update(add_to_headers)

    postdata = json.dumps(params)

    response = requests.post(url, postdata, headers=headers, timeout=10)
    log.Info(response.text)
    json_obj = json.loads(response.text)
    return json_obj

def pure_http_get_request(url):
    try:
        response = requests.get(url)
        if response is not None and response.status_code == 200:
            return response.text
        else:
            return None
    except:
        return None


def http_get_request(url, params = None, add_to_headers=None):
    headers = {
        "Content-type": "application/x-www-form-urlencoded",
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36',
    }
    if add_to_headers:
        headers.update(add_to_headers)
    if params == None:
        params = ''
    postdata = urllib.parse.urlencode(params)
    try:
        response = requests.get(url, postdata, headers=headers, timeout=20, verify=False)
        if response.status_code == 200:
            return response.json()
        else:
            return None
    except BaseException as e:
        return None


def tsplit(s, sep):
    stack = [s]
    for char in sep:
        pieces = []
        for substr in stack:
            pieces.extend(substr.split(char))
        stack = pieces
    return stack


def get_long_time(_str_time):
    # "2018-07-11 16:38:24.504"
    longtime = int("".join(tsplit(_str_time, ("-", " ", ":", "."))))
    return longtime


def get_passed_time_seconds(_now_time, _prev_time):
    result = (_now_time - _prev_time) / 1000.0
    result = get_precision(result, 3)
    return result


def shuffle_str(s):
    ls = list(s)
    random.shuffle(ls)
    return ''.join(ls)