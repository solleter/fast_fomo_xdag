from flask import Flask
import os
import json
from re import compile as re_compile

app = Flask(__name__)

_percent_pat = re_compile(r'(?:%[A-Fa-f0-9]{2})+')

def is_rpc_state_ok():
    command = """
    curl -s -H "Content-Type: application/json" --data "{\\"jsonrpc\\":\\"2.0\\",\\"method\\":\\"xdag_state\\",\\"params\\":[],\\"id\\":1}" 127.0.0.1:7677
    """
    try:
        result = os.popen(command).read().strip()
        result = "{" + result
        print("################################ RPC xdag_state: {}".format(result))
        json_obj = json.loads(result)
        state = json_obj["result"].strip()
        if state == "Connected to the mainnet pool. No mining.":
            return True
        else:
            return False
    except:
        print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% RPC xdag_state ERROR!")
        return False


def get_account_and_balance():

    if not is_rpc_state_ok():
        return None, None

    command = """
    curl -s -H "Content-Type: application/json" --data "{\\"jsonrpc\\":\\"2.0\\",\\"method\\":\\"xdag_get_account\\",\\"params\\":[],\\"id\\":1}" 127.0.0.1:7677
    """
    try:
        result = os.popen(command).read().strip()
        result = "{" + result
        print("################################ RPC xdag_get_account: {}".format(result))
        json_obj = json.loads(result)
        address_list = json_obj["result"]
        if len(address_list) > 0:
            address = address_list[0]["address"]
            balance = float(address_list[0]["balance"])
            return address, balance
        return None, None
    except:
        print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% RPC xdag_get_account ERROR!")
        return None, None


def send_to_address(_address, _amount):

    #if not is_rpc_state_ok():
    #    return None

    command = """
    curl -s -H "Content-Type: application/json" --data "{\\"jsonrpc\\":\\"2.0\\",\\"method\\":\\"xdag_do_xfer\\",\\"params\\":[{\\"amount\\":\\"$amount\\", \\"address\\":\\"$address\\"}],\\"id\\":1}" 127.0.0.1:7677
    """
    command = command.replace("$amount", str(_amount))
    command = command.replace("$address", _address)
    try:
        result = os.popen(command).read().strip()
        result = "{" + result
        print("################################ RPC xdag_do_xfer: {}".format(result))
        json_obj = json.loads(result)
        transaction_list = json_obj["result"]
        if len(transaction_list) > 0:
            return transaction_list[0]["block"]
        return None
    except:
        print("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% RPC xdag_do_xfer ERROR!")
        return None


def percent_decode(string):
    for substr in _percent_pat.findall(string):
        substr_dec = bytes.fromhex(
            substr.replace('%', '')).decode('utf-8')
        string = string.replace(substr, substr_dec)
    return string


@app.route('/')
def hello_world():
    return 'Hello, World!'


@app.route('/state')
def get_state():
    if is_rpc_state_ok():
        return "State OK."
    else:
        return "State Error!"


@app.route('/account')
def get_account():
    address, balance = get_account_and_balance()
    if address is not None and balance is not None:
        return "Account: {}  Balance: {}".format(address, balance)
    else:
        return "Get Account Error!"


"""
{"username":"fredshao","password":"mypassword","to_address":"xdag address","amount":0.143}
"""
@app.route('/transfer/<username_password>')
def do_transfer(username_password):
    username_password = percent_decode(username_password)
    try:
        json_obj = json.loads(username_password)
        username = json_obj["username"]
        password = json_obj["password"]

        if username != "fredshao" or password !="mypassword":
            return "Something wrong"

        to_address = json_obj["to_address"]
        amount = json_obj["amount"]

        to_address = to_address.replace(">","/")

        print("Send to {}  Amount:{}".format(to_address, amount))

        send_result = send_to_address(to_address, amount)
        if send_result is not None:
            return send_result
        else:
            return ""
    except:
        return "Something error."
    return "Something wrong"
