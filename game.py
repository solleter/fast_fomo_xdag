import util
from model import PlayObj, GameRoundObj, PaymentObj
import db
import model
import xdagrpc as rpc
import stringdist
import testagent
import log
import time
import threading
import view
import os

DEBUG = False

address_play = "0vFAaFwfrh9vnwhXHm0zx3cDKqRJJtrR"
address_dev_community = "0lVEZviziglmy4ivMogmNsOwrmmwqXsJ"
new_play_join_countdown_increase = 128
new_max_amount_of_play_join_countdown = 128 * 60
lucky_player_count = 20
min_join_amount = 10.0
system_exit = False
terminal_signal_file = "_terminate_"

"""
1. Get new play data from xdag explorer
2. Calculate runtime data
3. If the balance arrived wallet, Save new play data to database
4. Enumerate new play, and calculate the xdag allocation, (for prev players, for bonus pool, for dev community)
5. Save the xdag allocation to database, ready to payout
6. Payout every allocation and need check from explorer
"""

"""
What if?
1. What if the xdag explorer is down?
2. What if the xdag game server is down?
"""

"""
When a game of round will end?
1. The max amount of play stay 128 minutes, a round will end.

Some rules:
1. When a new max amount of play join game, the countdown will reset to 128 minutes.
2. If there is many same max amount of play, the first join is the real max amount of play.
3. When a new play join, the countdown will increase 128 seconds, the max of count down is 128 minutes.
"""


def is_xdag_blockchain_ok():
    """
    Make sure the xdag explorer data correct
    :return:
    """
    url = "https://explorer.xdag.io/api/status"
    result = util.http_get_request(url)
    if result is None or "error" in result:
        log.info("-------------------- 1 XDAG explorer data status ERROR: {}".format(result))
        return False
    if "version" in result:
        return True
    else:
        log.info("-------------------- 2 XDAG explorer data status ERROR: {}".format(result))
        return False


def check_game_round_end():
    un_end_play_list = db.get_un_end_play()
    count = len(un_end_play_list)
    if count == 0:
        return None, None, -1
    jackpot_play = None
    prev_play = None
    countdown = 0
    end_time = 0
    for index in range(0, count):
        check_play_obj = un_end_play_list[index]
        if jackpot_play is None or check_play_obj.amount > jackpot_play.amount:
            jackpot_play = check_play_obj
            countdown = new_max_amount_of_play_join_countdown
            prev_play = check_play_obj
            calculate_and_save_dividend_allocation(check_play_obj)
        else:
            passed_time = util.get_passed_time_seconds(check_play_obj.join_timestamp, prev_play.join_timestamp)
            if passed_time > countdown:
                end_time = check_play_obj.join_timestamp
                log.info("++++++++++++++++++++++++++++++++ 1 Game Round End: {}".format(end_time))
                break
            else:
                countdown -= passed_time
                countdown += new_play_join_countdown_increase
                if countdown > new_max_amount_of_play_join_countdown:
                    countdown = new_max_amount_of_play_join_countdown
                prev_play = check_play_obj
                calculate_and_save_dividend_allocation(check_play_obj)

    if jackpot_play is not None:
        log.info("JJJJJJJJJJJ Curr Jackpot Player: Play ID:{} Address: {} Amount:{}".format(
            jackpot_play.play_id, jackpot_play.player_address, jackpot_play.amount))

    if end_time == 0:
        passed_time = util.get_passed_time_seconds(util.get_now_utc_13timestamp(), prev_play.join_timestamp)
        log.info("Now UTC Timestamp: {}, Prev play join timestamp: {}, Prev play join amount: {}".format(
            util.get_now_utc_13timestamp(), prev_play.join_timestamp, prev_play.amount
        ))
        log.info("Passed Time: {}, Countdown: {}".format(passed_time, countdown))
        if passed_time > countdown:
            log.info("++++++++++++++++++++++++++++++++ 2 Game Round End, UTC Timestamp: {}, "
                     "prev play join timestamp: {}".format(util.get_now_utc_13timestamp(), prev_play.join_timestamp))
            end_time = util.get_now_utc_13timestamp()
            return end_time, jackpot_play, -1
        else:
            countdown -= passed_time
            return None, None, util.get_precision(countdown, 3)
    else:
        return end_time, jackpot_play, -1


def is_new_play(_transaction_address):
    """
    Check the play transaction address, if the address is not in the database,
    is new play, else it is not new play
    :param _transaction_address:
    :return: new Play -> True, not new Play -> False
    """
    return not db.has_play(_transaction_address)


def get_input_address(_transaction_address):
    """
    Get player address from transaction address
    :param _transaction_address:
    :return:
    """
    url = "https://explorer.xdag.io/api/block/{}".format(_transaction_address)
    try_count = 3
    while try_count > 0:
        try_count -= 1
        if DEBUG is True:
            result = testagent.get_test_new_play_input_address()
        else:
            result = util.http_get_request(url)
        if result is not None and "block_as_transaction" in result:
            try_count = 0
            if result["state"] != "Accepted":
                return None
            for item in result["block_as_transaction"]:
                if item["direction"] == "input":
                    address = item["address"]
                    # amount = item["amount"]
                    return address
        else:
            log.info("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! Get transaction input address error. "
                     "Transaction: {}; Result: {}".format(_transaction_address, result))
    return None


def get_new_play_data(_address):
    """
    Get the new join data about 4 battle teams, order by join timestamp desc
    :return:[{"play_id":"value", "player_address":"value","amount":"value","utc_time":"value"},...], True or False
    """
    url = "https://explorer.xdag.io/api/block/{}".format(_address)

    new_play_list = []
    completed_data = True

    try_count = 3
    while try_count > 0:
        try_count -= 1
        if DEBUG is True:
            result = testagent.get_test_new_play_data()
        else:
            result = util.http_get_request(url)
        if result is not None and "block_as_address" in result:
            try_count = 0
            completed_data = True
            for item in result["block_as_address"]:
                if item["direction"] == "input":
                    amount = item["amount"]

                    if float(amount) < min_join_amount:
                        continue

                    transaction_address = item["address"]
                    if not is_new_play(transaction_address):
                        continue
                    utc_time = item["time"]
                    input_address = get_input_address(transaction_address)
                    if input_address is None:
                        completed_data = False
                        continue
                    else:
                        new_play = dict()
                        new_play["play_id"] = transaction_address
                        new_play["player_address"] = input_address
                        new_play["amount"] = amount
                        new_play["utc_time"] = utc_time
                        new_play_list.append(new_play)
                        log.info("-------------------------------------------------------> "
                                 "New Play, play_id: {}, amount: {}".format(transaction_address, amount))
        else:
            completed_data = False
            log.info("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! Download transaction data Error. Result: {}".format(result))
    return new_play_list, completed_data


def save_new_play_list(_play_list):
    """
    Save data to database
    :param _play_list: [{"play_id":"value", "player_address":"value","amount":"value","utc_time":"value"},...]
    :param _team_id: 1, 2, 3, 4
    :return:
    """
    if _play_list is None or len(_play_list) == 0:
        return

    log.info("Save New Player List: {}".format(len(_play_list)))
    db_list = []
    for item in _play_list:
        new_play = PlayObj()
        new_play.play_id = item["play_id"]
        new_play.join_str_time = item["utc_time"]
        new_play.join_timestamp = util.utc_str_time_to_utc_13timestamp(new_play.join_str_time)
        new_play.join_longtime = util.get_long_time(new_play.join_str_time)
        new_play.player_address = item["player_address"]
        new_play.amount = float(item["amount"])
        new_play.dividend_allocated = False

        to_bonus_amount, to_prev_player_amount, to_dev_amount = model.calculate_dividend(new_play.amount)
        new_play.amount_to_bonus = to_bonus_amount
        new_play.amount_to_prev_player = to_prev_player_amount
        new_play.amount_to_dev = to_dev_amount

        db_list.append(new_play)
    db.save_play_list(db_list)


def get_balance(_address):
    """
    Get balance from explorer
    :param _address:
    :return:
    """
    url = "https://explorer.xdag.io/api/balance/{}".format(_address)
    log.info(url)
    result = util.http_get_request(url)
    if result is not None and "balance" in result:
        balance = float(result["balance"])
        return balance
    return None


def get_lucky_play_list(_un_end_play_list, _jackpot_play, _count):
    """
    Get Lucky player, first sort with join time,
    then according to the levenshtein distance, to select lucky player
    :param _un_end_play_list: [PlayObj]
    :param _jackpot_play: PlayObj, who won the jackpot
    :param _count: How many lucky player you want?
    :return:[PlayObj]
    """
    _list = []
    for play in _un_end_play_list:
        if play.play_id == _jackpot_play.play_id:
            continue
        data = {
            "obj": play,
            "distance": stringdist.levenshtein(_jackpot_play.play_id, play.play_id)
        }
        _list.append(data)

    _list.sort(key=lambda x: x["obj"].join_longtime)
    _list.sort(key=lambda x: x["distance"])

    result = []
    index = 0
    for item in _list:
        result.append(item["obj"])
        index += 1
        if index == _count:
            break
    return result


def get_start_play_from_list(_play_list):
    start_play = None
    for play in _play_list:
        if start_play is None or play.join_timestamp < start_play.join_timestamp:
            start_play = play
    return start_play


def prepare_for_payment(_end_timestamp, _jackpot_play):
    """
    Calculate the bonus allocation, to jackpot player, to dev, to lucky players
    Create bonus allocation obj and save to database, ready to xfer xdag
    :param _end_timestamp: the timestamp of game end(exclusive)
    :param _jackpot_play: The player who won the jackpot
    :return:
    """
    # if there is dividend allocation not end, do it.
    # prepare_for_dividend_allocate(_end_timestamp)
    # do_dividend_allocation_payment()
    un_end_play_list = db.get_un_end_play_before_timestamp(_end_timestamp)
    payment_list = []

    total_amount = 0.0
    dividend_to_prev_player_amount = 0.0
    dividend_to_dev_amount = 0.0
    dividend_to_bonus = 0.0
    game_round = db.curr_playing_round
    for play_obj in un_end_play_list:
        play_obj.play_end = True
        play_obj.round = game_round
        total_amount += play_obj.amount
        dividend_to_prev_player_amount += play_obj.amount_to_prev_player
        dividend_to_bonus += play_obj.amount_to_bonus
        dividend_to_dev_amount += play_obj.amount_to_dev

        # Create payment for each play dividend reward of this game round
        payment_obj = PaymentObj()
        payment_obj.payment_id = play_obj.play_id
        payment_obj.to_address = play_obj.player_address
        payment_obj.amount = play_obj.total_dividend_reward
        payment_obj.payment_type = 0
        payment_obj.round = game_round
        payment_list.append(payment_obj)

    total_amount = util.get_precision(total_amount, 6)
    dividend_to_prev_player_amount = util.get_precision(dividend_to_prev_player_amount, 6)
    dividend_to_bonus = util.get_precision(dividend_to_bonus, 6)
    dividend_to_dev_amount = util.get_precision(dividend_to_dev_amount, 6)

    bonus_from_prev_round = db.get_bonus_from_prev_round()
    total_bonus = dividend_to_bonus + bonus_from_prev_round

    # Create payment for dev dividend of this game round
    payment_obj = PaymentObj()
    payment_obj.payment_id = address_dev_community
    payment_obj.to_address = address_dev_community
    payment_obj.amount = dividend_to_dev_amount
    payment_obj.payment_type = 1
    payment_obj.round = game_round
    payment_list.append(payment_obj)

    # Start allocate bonus !
    bonus_allocation = model.bonus_allocation
    bonus_allocation.set_runtime_data(total_bonus)
    bonus_to_jackpot_amount = bonus_allocation.to_jackpot
    bonus_to_lucky_player_amount = bonus_allocation.to_lucky_players
    bonus_to_dev_amount = bonus_allocation.to_dev

    # Create payment of bonus for dev
    payment_obj = PaymentObj()
    payment_obj.payment_id = address_dev_community
    payment_obj.to_address = address_dev_community
    payment_obj.amount = bonus_to_dev_amount
    payment_obj.payment_type = 4
    payment_obj.round = game_round
    payment_list.append(payment_obj)

    # Create payment of bonus for lucky player
    lucky_play_list = get_lucky_play_list(un_end_play_list, _jackpot_play, lucky_player_count)
    if lucky_play_list is not None and len(lucky_play_list) > 0:
        total_lucky_play_amount = 0.0
        for lucky_play in lucky_play_list:
            total_lucky_play_amount += lucky_play.amount
        for lucky_play in lucky_play_list:
            play_amount = lucky_play.amount
            percentage = play_amount / total_lucky_play_amount
            percentage = util.get_precision(percentage, 3)
            bonus_reward = bonus_to_lucky_player_amount * percentage
            bonus_reward = util.get_precision(bonus_reward, 6)

            payment_obj = PaymentObj()
            payment_obj.payment_id = lucky_play.play_id
            payment_obj.to_address = lucky_play.player_address
            payment_obj.amount = bonus_reward
            payment_obj.payment_type = 3
            payment_obj.round = game_round
            payment_list.append(payment_obj)
    else:
        # if there is no lucky player, the amount of bonus to lucky player will give go jackpot player
        bonus_to_lucky_player_amount = 0
        bonus_to_jackpot_amount += bonus_to_lucky_player_amount

    # Create payment of bonus for jackpot player
    payment_obj = PaymentObj()
    payment_obj.payment_id = _jackpot_play.play_id
    payment_obj.to_address = _jackpot_play.player_address
    payment_obj.amount = bonus_to_jackpot_amount
    payment_obj.payment_type = 2
    payment_obj.round = game_round
    payment_list.append(payment_obj)

    # Create game round record
    start_play = get_start_play_from_list(un_end_play_list)
    game_round = GameRoundObj()
    game_round.round = db.curr_playing_round
    game_round.start_str_time = start_play.join_str_time
    game_round.start_timestamp = start_play.join_timestamp
    game_round.start_longtime = start_play.join_longtime
    game_round.total_play = len(un_end_play_list)
    game_round.total_join_amount = total_amount
    game_round.dividend_to_prev_player_amount = dividend_to_prev_player_amount
    game_round.dividend_to_dev_amount = dividend_to_dev_amount
    game_round.dividend_to_bonus = dividend_to_bonus
    game_round.bonus_from_prev_round = bonus_from_prev_round
    game_round.total_bonus_amount = total_bonus
    game_round.bonus_to_jackpot_amount = bonus_to_jackpot_amount
    game_round.bonus_to_lucky_player_amount = bonus_to_lucky_player_amount
    game_round.bonus_to_dev_amount = bonus_to_dev_amount
    game_round.bonus_to_next_round_amount = bonus_allocation.to_next_round
    game_round.jackpot_play_id = _jackpot_play.play_id
    game_round.jackpot_player_address = _jackpot_play.player_address
    game_round.jackpot_play_join_amount = _jackpot_play.amount
    game_round.end_str_time = util.utc_13timestamp_to_utc_str_time(_end_timestamp)
    game_round.end_timestamp = _end_timestamp

    db.save_play_list(un_end_play_list)
    db.save_payment_list(payment_list)
    db.save_game_round(game_round)


def calculate_and_save_dividend_allocation(_curr_play_obj):
    """
    Calculate dividend allocation for previous players and dev, and bonus
    Need xfer for prev players and dev, the other amount is for bonus.
    :param _curr_play_obj:
    :return:
    """
    if _curr_play_obj.dividend_allocated != 0:  # already dividend allocated
        return

    log.info("Do calculate dividend allocation: {}  Amount: {}".format(
        _curr_play_obj.play_id, _curr_play_obj.amount_to_prev_player))
    prev_play_list = db.get_un_end_play_before_timestamp(_curr_play_obj.join_timestamp)
    if len(prev_play_list) > 0:
        prev_total_amount = 0.0
        for prev_play_obj in prev_play_list:
            prev_total_amount += prev_play_obj.amount
        for prev_play_obj in prev_play_list:
            percentage = prev_play_obj.amount / prev_total_amount
            reward = _curr_play_obj.amount_to_prev_player * percentage
            reward = util.get_precision(reward, 6)
            total_reward = prev_play_obj.total_dividend_reward + reward
            total_reward = util.get_precision(total_reward, 6)
            prev_play_obj.total_dividend_reward = total_reward

    else:
        _curr_play_obj.amount_to_bonus += _curr_play_obj.amount_to_prev_player
        _curr_play_obj.amount_to_prev_player = 0

    _curr_play_obj.dividend_allocated = True
    prev_play_list.append(_curr_play_obj)
    db.save_play_list(prev_play_list)


def __payment_work_thread():
    log.info("Payment work thread started!")
    while True:
        if system_exit:
            break
        view.generate_payment_data()
        time.sleep(3)
        un_payment_list = db.get_un_payment_list()
        for payment in un_payment_list:
            amount = payment.amount
            if amount == 0:
                payment.transaction_id = "-"
            else:
                transaction_id = rpc.send_to_address(payment.to_address, amount)
                if transaction_id is not None:
                    payment.transaction_id = transaction_id
                    log.info(
                        "Payment to {} Amount:{} Transaction ID: {}".format(payment.to_address, amount, transaction_id))
                else:
                    log.info("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! Payment to {} Amount:{} ERROR! TransactionID: {}".format(
                        payment.to_address, amount, transaction_id
                    ))
            time.sleep(0.2)
            if system_exit:
                break
        db.save_payment_list(un_payment_list)
        if un_payment_list is not None and len(un_payment_list) > 0:
            log.info(">>>>>>>>>>>>>>>>>>>>>>>>>>>> !!!!!!!!!!!!!! Payment Down")
    log.info("----- Payment work thread exit!")


def __game_loop():
    """
    1. Save new play list
    2. check if the game is end, do end process [1. do dividend allocate, 2. do bonus allocate]
    2. if the game is not end, do dividend allocate
    :return:
    """
    log.info("Game loop work thread started!")
    while True:
        if system_exit:
            break
            
        if not is_xdag_blockchain_ok():
            model.update_surplus_time(-3)
            time.sleep(3)
            continue

        new_play_list, is_data_completed = get_new_play_data(address_play)

        if not is_data_completed:
            log.info("-------------------------------- Play data complete: {}, New players: {}.".format(
                is_data_completed, len(new_play_list)
            ))
        else:
            new_play_count = len(new_play_list)
            if new_play_count > 0:
                log.info("++++++++++++++++++++++++++++++++ New play joined, Count: {}".format(new_play_count))

        save_new_play_list(new_play_list)

        if is_data_completed:
            log.info("UTC Time: {}, Do game round end check ...".format(util.get_now_utc_13timestamp()))
            game_end_before_timestamp, jackpot_play, surplus_time = check_game_round_end()

            if game_end_before_timestamp is not None and jackpot_play is not None:

                log.info("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! Game Round End. "
                      "End timestamp: {}, "
                      "Jackpot play_id: {}, "
                      "Jackpot player address: {}, "
                      "Jackpot join amount: {} ".format(
                    game_end_before_timestamp,
                    jackpot_play.play_id,
                    jackpot_play.player_address,
                    jackpot_play.amount))

                prepare_for_payment(game_end_before_timestamp, jackpot_play)
                view.generate_history_data()
            else:
                if surplus_time >= 0:
                    model.update_surplus_time(surplus_time)
                    log.info(">>>>>>>>>>>>>>> Surplus Time: {}".format(surplus_time))
                else:
                    model.update_surplus_time(-1)
        else:
            log.info("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX Data not complete, Wait next time.")
            model.update_surplus_time(-2)
        time.sleep(3)

        view.generate_index_data()

    log.info("----- Game loop work thread exit!")


def __clear_terminal_signal():
    if os.path.exists(terminal_signal_file):
        os.remove(terminal_signal_file)


def __check_exit_work_thread():
    global system_exit
    while True:
        time.sleep(1)
        if os.path.exists(terminal_signal_file):
            system_exit = True
            break
    log.info("Set system exit signal!")


if __name__ == "__main__":
    __clear_terminal_signal()

    log.init_logger()
    model.init_model()
    db.init_database()

    time.sleep(1)
    view.generate_history_data()

    task_list = []
    # Start payment work thread
    t = threading.Thread(target=__payment_work_thread)
    t.start()
    task_list.append(t)

    t = threading.Thread(target=__game_loop)
    t.start()
    task_list.append(t)

    t = threading.Thread(target=__check_exit_work_thread)
    t.start()
    task_list.append(t)

    log.info("All work thread started, working...")
    for task in task_list:
        task.join()
    log.info("+++++++++++ All work thread exit, System Exit!")

    db.release_database()
    __clear_terminal_signal()

    # view.generate_index_data()
