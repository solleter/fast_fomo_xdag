import os
import json
import log
import util

is_remote_test = False


def is_rpc_state_ok():
    command = """
    curl -s -H "Content-Type: application/json" --data "{\\"jsonrpc\\":\\"2.0\\",\\"method\\":\\"xdag_state\\",\\"params\\":[],\\"id\\":1}" 127.0.0.1:7677
    """
    try:
        result = os.popen(command).read().strip()
        result = "{" + result
        log.info("################################ RPC xdag_state: {}".format(result))
        json_obj = json.loads(result)
        state = json_obj["result"].strip()
        if state == "Connected to the mainnet pool. No mining." or state == "Waiting for transfer to complete.":
            return True
        else:
            return False
    except:
        log.info("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% RPC xdag_state ERROR!")
        return False


def get_account_and_balance():

    if not is_rpc_state_ok():
        return None, None

    command = """
    curl -s -H "Content-Type: application/json" --data "{\\"jsonrpc\\":\\"2.0\\",\\"method\\":\\"xdag_get_account\\",\\"params\\":[],\\"id\\":1}" 127.0.0.1:7677
    """
    try:
        result = os.popen(command).read().strip()
        result = "{" + result
        log.info("################################ RPC xdag_get_account: {}".format(result))
        json_obj = json.loads(result)
        address_list = json_obj["result"]
        if len(address_list) > 0:
            address = address_list[0]["address"]
            balance = float(address_list[0]["balance"])
            return address, balance
        return None, None
    except:
        log.info("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% RPC xdag_get_account ERROR!")
        return None, None


def send_to_address(_address, _amount):
    if is_remote_test:
        _address = _address.replace("/",">")
        url = 'http://xdagpark.com:5000/transfer/{"username":"fredshao","password":"mypassword","to_address":"' + _address +'","amount":' + str(_amount) + '}'
        result = util.pure_http_get_request(url)
        if result != "":
            return result
        return None
    else:
        command = """
        curl -s -H "Content-Type: application/json" --data "{\\"jsonrpc\\":\\"2.0\\",\\"method\\":\\"xdag_do_xfer\\",\\"params\\":[{\\"amount\\":\\"$amount\\", \\"address\\":\\"$address\\"}],\\"id\\":1}" 127.0.0.1:7677
        """
        command = command.replace("$amount", str(_amount))
        command = command.replace("$address", _address)
        try:
            result = os.popen(command).read().strip()
            result = "{" + result
            log.info("################################ RPC xdag_do_xfer: {}".format(result))
            json_obj = json.loads(result)
            if "result" in json_obj:
                transaction_list = json_obj["result"]
                if len(transaction_list) > 0:
                    log.info("################################ Send to {} Amount: {} Successful: {}".format(_address, _amount, transaction_list[0]["block"]))
                    return transaction_list[0]["block"]
                log.info("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% Send to {} Amount: {} ERROR!".format(_address, _amount))
            return None
        except:
            log.info("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% RPC xdag_do_xfer ERROR!")
            return None



#print(send_to_address("qXfjj9/37vAp6kzXn/SsKms8NzUs/xaW",0.018))


