import os
import time

terminal_signal_file = "_terminate_"

with open(terminal_signal_file, "w") as f:
    f.write("")

while True:
    if not os.path.exists(terminal_signal_file):
        print("System successful exit!")
        break
    time.sleep(0.1)
