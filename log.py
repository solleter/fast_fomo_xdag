import logging
import os

from logging.handlers import TimedRotatingFileHandler

logger = None
inited = False


def info(_log_str):
    logger.info(_log_str)
    print(_log_str)


def init_logger():
    global inited

    if inited:
        return

    inited = True

    if not os.path.exists("./log"):
        os.mkdir("./log")

    global logger
    logFilePath = "log/info.log"
    logger = logging.getLogger("FomoXDAG")
    logger.setLevel(logging.INFO)

    handler = TimedRotatingFileHandler(logFilePath,
                                       when="MIDNIGHT",
                                       interval=1,
                                       backupCount=7)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    handler.setFormatter(formatter)

    logger.addHandler(handler)


init_logger()
