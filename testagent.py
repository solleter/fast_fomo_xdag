import random
import util
import time
import json
import os

address_str = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+-/"
length = len(address_str) - 1


def get_test_new_play_data():
    file = "./test/new_play.json"
    if not os.path.exists(file):
        return None
    with open(file, "r", encoding="utf-8") as f:
        json_str = f.read()
        if json_str != "":
            obj = json.loads(json_str)
            return obj
        else:
            return None
    return None


def get_test_new_play_input_address():
    return {
        "state": "Accepted",
        "block_as_transaction": [
            {
                "direction": "input",
                "address": get_random_address(),
            }
        ]
    }


def generate_test_time():
    now_time_str = util.get_current_strtime() + "." + str(random.randint(100, 999))
    return now_time_str


def generate_random_new_play(_amount):
    return {
        "direction": "input",
        "address": get_random_address(),
        "amount": _amount,
        "time": generate_test_time()
    }


def generate_test_data(_amount_list):

    data = get_test_new_play_data()

    if data is None:
        data = {"block_as_address": []}

    for amount in _amount_list:
        new_play = generate_random_new_play(amount)
        data["block_as_address"].append(new_play)
        wait_time = random.random() * 2.0
        print("New Play: ", amount, "Wait: ", wait_time)
        #time.sleep(wait_time)

    json_str = json.dumps(data)
    file = "./test/new_play.json"
    with open(file, "w", encoding="utf-8") as f:
        f.write(json_str)


def get_random_address():
    address_ch_list = []
    for x in range(32):
        rand_index = random.randint(0, length)
        ch = address_str[rand_index: rand_index + 1]
        address_ch_list.append(ch)
    _address = "".join(address_ch_list)
    _address = util.shuffle_str(_address)
    return _address

